//todo #define NDEBUG
#include <assert.h>
#include <vector>
#include "Single.h"

Single::Single(std::string name, SINGLE min, SINGLE max)
{
	_single=min;
	singleToBytes(min, _minBytes);
	singleToBytes(max, _maxBytes);
	_updated=true;
	_name = name;
	_bytes.resize(SINGLE_BYTES);
	_dataType = FLOAT_DATA_TYPE;
}

std::vector<UC8> &Single::getBytes()
{
	if (_updated)
	{
		Lock();
		UC8 *ptr =(UC8*)&_single;
		for (int i = 0; i < SINGLE_BYTES; ++i)
		{
			_bytes[i] = ptr[i];
		}
		_updated = false;
		Unlock();
	}
	return _bytes;
}

void Single::update(SINGLE single)
{
	if (single != _single)
	{
		Lock();
		_updated = true;
		_single = single;
		Unlock();
	}
}

void Single::operator=(SINGLE single)
{
	update(single);
}

void Single::singleToBytes(SINGLE single, std::vector<UC8> &bytes)
{
	bytes.resize(SINGLE_BYTES);
	UC8 *ptr =(UC8*)&single;
	for (int i = 0; i < SINGLE_BYTES; ++i)
	{
		bytes[i] = ptr[i];
	}
}