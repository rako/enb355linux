#ifndef _SINGLE_H_
#define _SINGLE_H_

#include "Object.h"
#include "DataTypes.h"

class Single : public Object
{
protected:
	SINGLE _single;
	void singleToBytes(SINGLE single, std::vector<UC8> &bytes);
public:
	Single(std::string name, SINGLE min = std::numeric_limits<float>::min(), SINGLE max = std::numeric_limits<float>::max());
	std::vector<UC8> &getBytes();
	void update(SINGLE single);
	void operator=(SINGLE single);
};

#endif
