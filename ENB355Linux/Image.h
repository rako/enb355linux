#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <string>
#include <vector>
#include "Object.h"
#include "DataTypes.h"

class Image : public Object
{
public:
	Image(std::string name);
	std::vector<UC8> &getBytes();
	void update(std::vector<UC8> image);
	void update(std::string fileName);
};

#endif
