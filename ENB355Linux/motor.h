#ifndef _MOTOR_H_
#define _MOTOR_H_

namespace Motor
{
	void motorThread();
	void updateLeftMotor(float leftMotor);
	void updateRightMotor(float rightMotor);
	void updateVericalMotor(float verticalMotor);
	//void updateServo(float servo);
	void deliverPayload();
};

#endif
