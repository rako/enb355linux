//todo #define NDEBUG
#include <assert.h>
#include <vector>
#include "String.h"
#include "Helper.h"

String::String(std::string name) 
{
	_name = name;
	_string = "";
	_updated = true;
	_dataType = STRING_DATA_TYPE;
}

std::vector<UC8> &String::getBytes()
{
	if (_updated)
	{
		Lock();
		Helper::stringToBytes(_string, _bytes);
		_updated = false;
		Unlock();
	}
	return _bytes;
}

void String::update(std::string string)
{
	if (string != _string)
	{
		Lock();
		assert (_string.length() <= UC8_MAX);
		_updated = true;
		_string = string;
		Unlock();
	}
}

void String::operator=(std::string string)
{
	update(string);
}