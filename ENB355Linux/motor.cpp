//============================================================================
// Name        : mot.cpp
// Author      : Fazl Al Abodi
// Version     : 1.1
//============================================================================
 
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <unistd.h>
#include <boost/thread.hpp>
#include "motor.h"

boost::mutex lock;
float _leftMotor;
float _rightMotor;
float _verticalMotor;
float _servo;

bool payloadDelivered = false;

int left(int);
int right(int);
int alt(int);
int servo(int);
int floatToInt(float, int, int);

void Motor::motorThread()
{
	int freq = 400;
	int O_speed = 50;


	std::ofstream myfile;

	myfile.open("/sys/class/pwm/ehrpwm.1:0/request");
	myfile << 0;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.1:0/period_freq");
	myfile << freq;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.1:0/duty_percent");
	myfile << O_speed;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.1:0/run");
	myfile << 1;
	myfile.close();



	myfile.open("/sys/class/pwm/ehrpwm.1:1/request");
	myfile << 0;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.1:1/period_freq");
	myfile << freq;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.1:1/duty_percent");
	myfile << O_speed;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.1:1/run");
	myfile << 1;
	myfile.close();



	myfile.open("/sys/class/pwm/ehrpwm.0:0/request");
	myfile << 0;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.0:0/period_freq");
	myfile << freq;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.0:0/duty_percent");
	myfile << O_speed;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.0:0/run");
	myfile << 1;
	myfile.close();



	myfile.open("/sys/class/pwm/ehrpwm.0:1/request");
	myfile << 0;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.0:1/period_freq");
	myfile << freq;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.0:1/duty_percent");
	myfile << O_speed;
	myfile.close();

	myfile.open("/sys/class/pwm/ehrpwm.0:1/run");
	myfile << 1;
	myfile.close();
	
	// work area =====================================================
	int L_percent=50;
	int R_percent=50;
	int A_percent=50;
	int S_percent=0;
	
	int mMIN = 50;
	int mMAX = 75;
	
	int sMIN = 0;
	int sMAX = 10;

	servo(0);

	//while (1)
	//{

		//L_percent = floatToInt(_leftMotor, mMIN, mMAX);
		//R_percent = floatToInt(_rightMotor, mMIN, mMAX);
		//A_percent = floatToInt(_verticalMotor, mMIN, mMAX);

		left(L_percent);

		right(R_percent);

		alt(A_percent);
		//std::cout << "A: " << A_percent << ", L:" << L_percent << ", R: " << R_percent <<std::endl;
		//servo(S_percent);
		//boost::this_thread::sleep(boost::posix_time::milliseconds(20));
	//}

	// work area ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
}

void Motor::deliverPayload()
{
	if (!payloadDelivered)
	{
		std::cout << "Payload delivering" << std::endl;
		servo(10);
		usleep(150000L);
		servo(0);
		payloadDelivered = true;
	}
}

int floatToInt(float in, int mMIN, int mMAX)
{
	int out = mMIN;
	if (in > 0)
	{
		out += in * (mMAX - mMIN);
	}
	return out;
}

void Motor::updateLeftMotor(float leftMotor)
{
	left(floatToInt(leftMotor, 50, 90));
}


void Motor::updateRightMotor(float rightMotor)
{
                right(floatToInt(rightMotor, 50, 90));
}


void Motor::updateVericalMotor(float verticalMotor)
{
	alt(floatToInt(verticalMotor, 50, 90));
}

/*void updateServo(float servo)
{
	lock.lock();
	_servo = servo;
	lock.unlock();
}*/

int left(int percent) {
	std::ofstream myfile;
	myfile.open("/sys/class/pwm/ehrpwm.1:0/duty_percent");
	myfile << percent;
	myfile.close();
	return 0;

}

int right(int percent) {
	std::ofstream myfile;
	myfile.open("/sys/class/pwm/ehrpwm.1:1/duty_percent");
	myfile << percent;
	myfile.close();
	return 0;

}

int alt(int percent) {
	std::ofstream myfile;
	myfile.open("/sys/class/pwm/ehrpwm.0:0/duty_percent");
	myfile << percent;
	myfile.close();
	return 0;

}

int servo(int percent) {
	std::ofstream myfile;
	myfile.open("/sys/class/pwm/ehrpwm.0:1/duty_percent");
	myfile << percent;
	myfile.close();
	return 0;

}
