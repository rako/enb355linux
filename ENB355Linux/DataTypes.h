#ifndef _DATATYPES_H_
#define _DATATYPES_H_

typedef unsigned char UC8;
typedef signed char SC8;
typedef unsigned short UI16;
typedef short SI16;
typedef unsigned int UI32;
typedef int SI32;
typedef unsigned long long UI64;
typedef long SI64;
typedef float SINGLE;

#define UC8_MAX 255
#define UC8_MIN 0
#define UI32_BYTES 4
#define SI32_BYTES 4
#define SINGLE_BYTES 4

#define SI32_MIN INT_MIN
#define SI32_MAX INT_MAX

#define SINGLE_MIN FLT_MIN
#define SINGLE_MAX FLT_MAX

#endif
