//todo #define NDEBUG
#include <assert.h>

#include "Object.h"
#include "Helper.h"

void Object::Lock()
{
	lock.lock();
}

void Object::Unlock()
{
	lock.unlock();
}

DataType Object::getDataType()
{
	return _dataType;
}

std::vector<UC8> &Object::getNameBytes()
{
	if (_name.size() + 1 != _nameBytes.size())
	{
		Helper::stringToBytes(_name, _nameBytes);
	}
	return _nameBytes;
}

std::vector<UC8> &Object::getMinBytes()
{
	return _minBytes;
}

std::vector<UC8> &Object::getMaxBytes()
{
	return _maxBytes;
}

std::string Object::getName()
{
	return _name;
}