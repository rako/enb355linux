#include "client.h"

Client::~Client()
{
	_socket->close();
	delete _socket;
}

bool Client::getIsActive()
{
	return _isActive;
}

std::string Client::getName()
{
	return _name;
}

std::string Client::getIP()
{
	return _ip;
}

boost::asio::ip::tcp::socket* Client::getSocket()
{
	return _socket;
}

std::list<std::vector<UC8> >::iterator Client::getLastCommand()
{
    return _lastCommand;
}

void Client::setLastCommand(std::list<std::vector<UC8> >::iterator lastCommand)
{
    _lastCommand = lastCommand;
}
