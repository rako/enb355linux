#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#include <list>
#include <vector>
#include <string>
#include "DataTypes.h"

enum CommandSource
{
    GCS = 0x00,
    Blimp = 0x01
};

class Commands
{
protected:
	std::list<std::vector<UC8> > _commands;
public:
	~Commands();
	void add(CommandSource source, std::string message);
	std::vector<UC8> toBytes(std::list<std::vector<UC8> >::iterator &lastCommand);
	std::list<std::vector<UC8> >::iterator badCommand();
};

#endif
