#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <boost/asio.hpp>
#include <string>
#include <vector>
#include <list>
#include <iterator>
#include "DataTypes.h"

class Client
{
protected:
	//tcpclient
	bool _isActive;
	std::string _name;
    std::string _ip;
	boost::asio::ip::tcp::socket* _socket;
	std::list<std::vector<UC8> >::iterator _lastCommand;

public:
	Client(boost::asio::ip::tcp::socket* socket, bool active, std::string name, std::string ip, std::list<std::vector<UC8> >::iterator lastCommand) : _socket(socket), _ip(ip), _isActive(active), _name(name), _lastCommand(lastCommand) {}
	~Client();
	bool getIsActive();
	std::string getName();
	std::string getIP();
	std::list<std::vector<UC8> >::iterator getLastCommand();
	void setLastCommand(std::list<std::vector<UC8> >::iterator lastCommand);
	boost::asio::ip::tcp::socket* getSocket();
protected:

};

#endif
