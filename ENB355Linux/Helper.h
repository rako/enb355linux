#ifndef _HELPER_H_
#define _HELPER_H_

#include <string>
#include <vector>
#include "DataTypes.h"


namespace Helper
{
	void stringToBytes(std::string string, std::vector<UC8> &bytes, int offset = 0);
	std::string bytesToString(const std::vector<UC8> &bytes, int offset = 0);
	void copyVectorToVector(std::vector<UC8> &copyTo, const std::vector<UC8> &copyFrom,UI32 &offset);
	void copyVectorToBackOfVector(std::vector<UC8> &copyTo, const std::vector<UC8> &copyFrom,UI32 &size);
	//void copyUI16ToVector(std::vector<UC8> &copyTo, const UI16 &copyFrom,UI32 &offset);
	//void copyUI32ToVector(std::vector<UC8> &copyTo, const UI32 &copyFrom,UI32 &offset);
	
	template <class T>
	void copyXToVector (std::vector<UC8> &copyTo, const T &copyFrom,UI32 &offset)
	{
		UC8* ptr = (UC8*)&copyFrom;
		for (int i = 0; i < sizeof(T); ++i)
		{
			copyTo[offset] = *ptr;
			++ptr;
			++offset;
		}
	}

	template <class T>
	T getXfromVector (std::vector<UC8> &copyFrom, UI32 &offset)
	{
		T output;
		UC8* ptr = (UC8*)&output;
		for (int i = 0; i < sizeof(T); ++i)
		{
			ptr[i] = copyFrom[offset];
			++offset;
		}
		return output;
	}
};

#endif
