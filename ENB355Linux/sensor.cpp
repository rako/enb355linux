//============================================================================
// Name        : Sensor.cpp
// Author      : Fazl Al Abodi
// Version     : 1.0
// Description : Sensor calculations in C++
//============================================================================

#include <iostream>
#include <stdio.h>
#include "sensor.h"
using namespace std;

int Sensors::sensorMain()
{
    front();
    right();
    left();
    top();

    return 0;
}

double Sensors::front() {
    double front = sensor("/sys/devices/platform/omap/tsc/ain1");;
    return front;

}

double Sensors::right() {
    double right = sensor("/sys/devices/platform/omap/tsc/ain2");;
    return right;

}

double Sensors::left() {
    double left = sensor("/sys/devices/platform/omap/tsc/ain3");;
    return left;

}

double Sensors::top() {
    double top = sensor("/sys/devices/platform/omap/tsc/ain4");
    return top;
}


double Sensors::sensor(std::string fileName)
{
    double sensorNum = 0;
    int num;
    double numN;

    for (int i = 0; i < 100; i++) {
        FILE *pFile;
        pFile = fopen(fileName.c_str(), "r");
        if (pFile != NULL) {
            fscanf(pFile, "%d", &num);
            numN = (num / 4096.0) * 1.8;
            numN = (numN * 2) + 0.09;
            numN = (89.1460385 * (numN * numN * numN * numN))
                    - (474.0714 * (numN * numN * numN))
                    + (943.850509 * (numN * numN)) - (874.72463 * numN)
                    + 374.351111;
            fclose(pFile);
            sensorNum = sensorNum + numN;
        } else {
            //cout << "Sensor Not Available" << endl;
            sensorNum = -100;
            break;
        }
    }
    sensorNum = sensorNum / 100;
    //cout << ">>Top<< Sensor Distance Reading:" << sensorNum << "cm" << endl;
    return sensorNum;
}
