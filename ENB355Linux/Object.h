#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <vector>
#include <boost/thread.hpp>
#include "DataTypes.h"

enum DataType
{
    INT_DATA_TYPE = 0x01,
    FLOAT_DATA_TYPE = 0x02,
    STRING_DATA_TYPE = 0x88,
	IMAGE_DATA_TYPE = 0XC0
};

class Object
{
protected:
	bool _updated; // true when data has been updated sinc last generation of bytes otherwise false
	std::vector<UC8> _bytes;
	std::string _name;
	std::vector<UC8> _minBytes;
	std::vector<UC8> _maxBytes;
	std::vector<UC8> _nameBytes;
	boost::mutex lock;
	DataType _dataType;



	

public:
	virtual std::vector<UC8> &getBytes() = 0;
	void Lock();
	void Unlock();
	std::string getName();
	DataType getDataType();
	//todo check if vectors should be returned as a reference and not a value
	std::vector<UC8> &getNameBytes();
	std::vector<UC8> &getMinBytes();
	std::vector<UC8> &getMaxBytes();
};

#endif