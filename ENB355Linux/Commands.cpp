#include <ctime>
#include <boost/utility.hpp>
#include <string>
#include "Commands.h"
#include "Helper.h"

Commands::~Commands()
{
	for(std::list<std::vector<UC8> >::iterator i = _commands.begin(); i!=_commands.end(); ++i) {
		//delete[] (*i);
	}
}

void Commands::add(CommandSource source, std::string message)
{
	//todo
	char Time[9];
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	strftime(Time, 255, "%X", timeinfo);
	
	//add time
    //message = DateTime.Now.ToString("HH:mm:ss") + message;
	std::string meesageWithTime = Time + message;
	std::vector<UC8> byteArray;
	Helper::stringToBytes(meesageWithTime, byteArray, 1);
	byteArray[0] = (UC8)source;

    //add to internal storage
    _commands.push_back(byteArray);
}

std::vector<UC8> Commands::toBytes(std::list<std::vector<UC8> >::iterator &lastCommand)
{
	std::vector<UC8> byteArray;
	//check if no more or if not yet started
    if (lastCommand == _commands.end())
    {
        lastCommand = _commands.begin();
    }
    else if (boost::next(lastCommand) != _commands.end())
    {
        lastCommand = boost::next(lastCommand);
    }
    else
    {
        return byteArray;
    }

    std::list<std::vector<UC8> >::iterator tempLastCommand = lastCommand;
    int numBytes = 0;
    UI16 numCommandas = 0;
    bool loop = true;
    do
    {
        numBytes += lastCommand->size();
        numCommandas++;
        if (boost::next(lastCommand) != _commands.end())
        {
            lastCommand++;
        }
        else
        {
            loop = false;
        }
    }
    while (loop);

    byteArray.resize(numBytes + 2);
    UI32 pos = 0;
	Helper::copyXToVector<UI16>(byteArray, numCommandas, pos);

    for (int i = 0; i < numCommandas; ++i)
    {
		Helper::copyVectorToVector(byteArray, *tempLastCommand, pos);
        tempLastCommand++;
    }

    return byteArray;
}

std::list<std::vector<UC8> >::iterator Commands::badCommand()
{
    return _commands.end();
}
