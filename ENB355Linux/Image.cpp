//todo #define NDEBUG
#include <assert.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "Image.h"
#include "Helper.h"

Image::Image(std::string name) 
{
	_name = name;
	_dataType = IMAGE_DATA_TYPE;
}

std::vector<UC8> &Image::getBytes()
{
	return _bytes;
}

void Image::update(std::vector<UC8> image)
{
	Lock();
	_bytes.resize(image.size() + UI32_BYTES);
	UI32 offset = 0;
	Helper::copyXToVector<UI32>(_bytes, (UI32)image.size(), offset);
	Helper::copyVectorToVector(_bytes, image, offset);
	Unlock();
}

void Image::update(std::string fileName)
{
	Lock();
	_bytes.resize(UI32_BYTES);
	UI32 offset = 0;
	std::ifstream is;

	is.open (fileName.c_str());        // open file
	UC8 In;
	while (is.good())     // loop while extraction from file is possible
	{
		In = is.get();       // get character from file
		if (is.good())
		{
			_bytes.push_back(In);
		}
	}
	is.close();           // close file
	Helper::copyXToVector<UI32>(_bytes, (UI32)(_bytes.size()-UI32_BYTES), offset);
	Unlock();
}
