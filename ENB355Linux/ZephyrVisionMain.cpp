#include "ZephyrVisionMain.h"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O
#include "BlobResult.h"
#include "Blob.h"
#include "Server.h"
#include "motor.h"
#include <cv.h>
using namespace std;

//#define DEBUG
int deliverTo = -1;

void survivorToDeliver(int num)
{
	deliverTo = num;
}

bool survivorFound[NUM_SURVIVORS] = {false, false, false};

void survivorInfo(bool *survivor[])
{
	*survivor = survivorFound;
}

void outputStreamImage(IplImage* image) {
	cvSaveImage("/tmp/ram/out.jpg", image); // The path to where to save the image
	Server::getTheImage()->update("/tmp/ram/out.jpg");
}

 int getChannelValue(IplImage* image, int xPixelValue, int yPixelValue, int channel) {
	 int channelValue = ((uchar *)(image->imageData + xPixelValue*image->widthStep))[yPixelValue*image->nChannels + channel];
	 return channelValue;
 }

 void setChannelValue(IplImage* image, int xPixelValue, int yPixelValue, int channel, int value) {
	 ((uchar *)(image->imageData + xPixelValue*image->widthStep))[yPixelValue*image->nChannels + channel] = value;
 }

 void changeContrastBrightness(IplImage* inputImage, IplImage* outputImage, double contrast, int brightness) {
	for(int i = 0; i < inputImage->height; i++) {
		for(int j = 0; j < inputImage->width; j++) {
			int pixelRedValue = getChannelValue(inputImage, i, j, 2);
			setChannelValue(outputImage, i, j, 2, contrast*pixelRedValue+brightness);
			int pixelGreenValue = getChannelValue(inputImage, i, j, 1);
			setChannelValue(outputImage, i, j, 1, contrast*pixelGreenValue+brightness);
			int pixelBlueValue = getChannelValue(inputImage, i, j, 0);
			setChannelValue(outputImage, i, j, 0, contrast*pixelBlueValue+brightness);
		}
	}
 }

 bool detDimensions(CBlob* blob, int imgSurvivorWidth, int imgSurvivorHeight) {
 	bool expectedWidth = false;
 	bool expectedHeight = false;
 	if((blob->MaxX()-blob->MinX()) >= imgSurvivorWidth) {
 		expectedWidth = true;
 	}
 	if((blob->MaxY()-blob->MinY()) >= imgSurvivorHeight) {
 		expectedHeight = true;
 	}
 	if(expectedWidth == true && expectedHeight == true) {
 		return true;
 	}
 	else {
 		return false;
 	}
 }

int detSurvivors(IplImage* frame, IplImage* displayedImage, int i ) {
	CBlobResult blobs;
	CBlob biggestBlob;
	int survivorWidth = 50;
	int survivorHeight = 50;
	blobs = CBlobResult( displayedImage, NULL, 0 );
	// blobs.Filter( blobs, B_EXCLUDE, CBlobGetArea(), B_LESS, 100 );
	CBlobGetMean getMeanColor( frame );
	blobs.GetNthBlob( CBlobGetArea(), 0, biggestBlob );
	// std::cout << biggestBlob.MaxX() << ", " << biggestBlob.MaxY() << std::endl;
	int xLength = biggestBlob.MaxX()-biggestBlob.MinX();
	int yLength = biggestBlob.MaxY()-biggestBlob.MinY();
	//std::cout << xLength << ", " << yLength << " colour: " << i << std::endl;
	if(xLength >= survivorWidth && yLength >= survivorHeight) {
		std::cout << "Survivor of type: " << i << " was found." << std::endl;
		survivorFound[i] = {true};
		if (deliverTo == i)
		{
			Motor::deliverPayload();
		}
		return 1;
	}
	// detColor(biggestBlob, frame, frame);
	// detDimensions(biggestBlob, survivorWidth, survivorHeight);
	else {
		return 0;
	}
}

int filter(int r, int g, int b, int threshold, int color[]) {
	// Colors are in HSV
    int diff =  (color[0]-r)*(color[0]-r) +
                (color[1]-g)*(color[1]-g) +
                (color[2]-b)*(color[2]-b);

    if(diff < threshold) return (diff-threshold);
    return 0;
}

void createMask(IplImage* inputImage, IplImage* outputImage, int color[]) {
    for(int i = 0; i < inputImage->height; i++) {
        for(int j = 0; j < inputImage->width; j++) {
            int r = ((uchar *)(inputImage->imageData + i*inputImage->widthStep))[j*inputImage->nChannels + 2];
            int g = ((uchar *)(inputImage->imageData + i*inputImage->widthStep))[j*inputImage->nChannels + 1];
            int b = ((uchar *)(inputImage->imageData + i*inputImage->widthStep))[j*inputImage->nChannels + 0];
            int f = filter(r, g, b, 4800, color);
            if(f) {
                //((uchar *)(inputImage->imageData + i*inputImage->widthStep))[j*inputImage->nChannels + 2] = color[0];
                //((uchar *)(inputImage->imageData + i*inputImage->widthStep))[j*inputImage->nChannels + 1] = color[1];
                //((uchar *)(inputImage->imageData + i*inputImage->widthStep))[j*inputImage->nChannels + 0] = color[2];
                ((uchar *)(outputImage->imageData + i*outputImage->widthStep))[j] = 255;
            }
        }
    }
}

// The test application starts here
void mainImage() {
	// Initialization
#ifdef DEBUG
	char wndname[] = "Blobs";
	cvNamedWindow( name, CV_WINDOW_AUTOSIZE );
#endif
	CvCapture* capture = cvCaptureFromCAM( CV_CAP_ANY );
	IplImage* frame = 0;
	IplImage* displayedImage;
	int yellow[3] = {205, 205, 0};
	int green[3] = {34, 139, 34};
	int pink[3] = {175, 0, 95};
	int colors[3][3] = {{yellow[0], yellow[1], yellow[2]},
			{green[0], green[1], green[2]},
			{pink[0], pink[1], pink[2]}};
	// Grab frames until program termination
	while(1) {
		frame = cvQueryFrame( capture );
		outputStreamImage(frame);
		displayedImage = cvCreateImage(cvSize(640, 480), IPL_DEPTH_8U,3);
		// changeContrastBrightness(frame, displayedImage, 1.0, 0);
		// Goes through the image three times, one for each coloured survivor
		for(int i = 0; i < 3; i++) { // 0 for yellow, 1 for green and 2 for pink
			createMask(frame, displayedImage, colors[i]);
			if(detSurvivors(frame, displayedImage, i)) {
				// Notifies the survivor of a particular type has been found
				break;
			}
		}
		if( !frame ) break;
	#ifdef DEBUG
		cvShowImage( name, frame );
		cvShowImage( wndname, displayedImage );
	#endif
		char c = cvWaitKey(33);
		if( c == 27 ) break;
		cvReleaseImage( &displayedImage );
		// Sleep(100); Can be used to reduce processing power requirements
	}
	// Cleanup
	cvReleaseCapture( &capture );
#ifdef DEBUG
	cvDestroyWindow( name );
	cvDestroyWindow( wndname );
#endif
}
