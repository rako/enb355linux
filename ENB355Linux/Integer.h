#ifndef _INTEGER_H_
#define _INTEGER_H_

#include "Object.h"
#include "DataTypes.h"

class Integer : public Object
{
protected:
	SI32 _integer;
	void intToBytes(SI32 integer, std::vector<UC8> &bytes);
public:
	Integer(std::string name, SI32 min = SI32_MIN, SI32 max = SI32_MAX);
	std::vector<UC8> &getBytes();
	void update(SI32 integer);
	void operator=(SI32 integer);
};

#endif