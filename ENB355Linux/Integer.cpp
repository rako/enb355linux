//todo #define NDEBUG
#include <assert.h>
#include <vector>
#include "Integer.h"

Integer::Integer(std::string name, SI32 min, SI32 max) 
{

	_integer=min;
	intToBytes(min, _minBytes);
	intToBytes(max, _maxBytes);
	_name = name;
	_updated=true;
	_bytes.resize(SI32_BYTES);
	_dataType = INT_DATA_TYPE;
}

std::vector<UC8> &Integer::getBytes()
{
	if (_updated)
	{
		Lock();
		intToBytes(_integer, _bytes);
		_updated = false;
		Unlock();
	}
	return _bytes;
}



void Integer::update(SI32 integer)
{
	if (integer != _integer)
	{
		Lock();
		_updated = true;
		_integer = integer;
		Unlock();
	}
}

void Integer::operator=(SI32 integer)
{
	update(integer);
}

void Integer::intToBytes(SI32 integer, std::vector<UC8> &bytes)
{
	bytes.resize(SI32_BYTES);
	UC8 *ptr =(UC8*)&integer;
	for (int i = 0; i < SI32_BYTES; ++i)
	{
		bytes[i] = ptr[i];
	}
}