#ifndef _SERVER_H_
#define _SERVER_H_

#include <vector>
#include <list>
#include <memory>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "Object.h"
#include "client.h"
#include "Image.h"
#include "Commands.h"

//#define ObjectPtr std::unique_ptr<Object>
//#define ObjectVector std::vector<ObjectPtr>

typedef boost::shared_ptr<Object> ObjectPtr;
typedef std::vector<ObjectPtr> ObjectVector;

#define ADMIN_GCS_NAME "Group 4 - GCS"
#define KEEP_ALIVE_TIME_IN_SECONDS	5

#define FRONT_OFFSET	(-30)
#define SIDE_OFFSET	(-50)
#define BOTTOM_OFFSET	(-15)

enum PacktHeader
{
    LoginRequest = 0x00,
    ReplyLoginRequest = 0x01,
    SensorDataStructure = 0x02,
    SensorData = 0x03,
    CommandsPacket = 0x04,
    CommandBlimp = 0x40,
    Deliver = 0x41,
    Survivor = 0x42,

	Control = 0x43,
	DeliverTo = 0x44
    //todo add to documentation survivor packet
};

enum ErrorCodes
{
    NoError = 0,
    Invalid = 1,
    UnknownTypeOfPacket = 2,
    DataPacketSizeMismatch = 3,
    AdminLogin = 44,
};

class Server
{
protected:
	ObjectVector _data;
	std::vector<UC8> _setUpPacket;
	std::vector<UC8> _dataPacket;
	bool terminate;

	boost::shared_ptr<boost::thread> listener;
	boost::shared_ptr<boost::thread> sender;
	boost::shared_ptr<boost::thread> admin;
	boost::shared_ptr<boost::thread> update;

	std::list<boost::shared_ptr<Client> > clients;

	boost::shared_ptr<Client> adminClient;

	Commands commandLog;
	//boost::mutex lock;

	int randNum;

	static boost::shared_ptr<Image> _theImage;

public:
	Server();
	void stop();
	static Image* getTheImage();

protected:
	void listnerThread();
	void sendThread();
	void adminThread();
	void updateThread();


	void setUpData();
	void generateSetUpPacket();

	void generateDataPacket();

	std::vector<UC8> generateCommandsPacket(std::list<std::vector<UC8> >::iterator &lastCommand);

	void HandleClientComm(boost::asio::ip::tcp::socket *socket);

	UC8 processnewClient(boost::asio::ip::tcp::socket *socket, std::string &ClientName);

	//std::vector<UC8> read(boost::asio::ip::tcp::socket *socket, int size);
};

#endif
