/*
 * sensor.h
 *
 *  Created on: 18/09/2012
 *      Author: craig
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include <string>

namespace Sensors
{
    int sensorMain();

    double front();
    double right();
    double left();
    double top();

    double sensor(std::string fileName);
}

#endif /* SENSOR_H_ */
