//ZephyrVisionMain.h
#ifndef _ZEPHYR_VISION_MAIN_H_
#define _ZEPHYR_VISION_MAIN_H_

#define NUM_SURVIVORS	3

void mainImage();
void survivorInfo(bool* survivor[]);
void survivorToDeliver(int num);

#endif
