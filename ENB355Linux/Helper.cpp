//todo #define NDEBUG
#include <assert.h>
#include "Helper.h"

void Helper::stringToBytes(std::string string, std::vector<UC8> &bytes, int offset)
{
	int size = string.length();
	assert (size <= UC8_MAX);
	bytes.resize(size + 1 + offset);
	int pos = + offset;
	bytes[pos] = (UC8)size;
	pos+=1;
	for (int i = 0; i < size; ++i)
	{
		bytes[pos] = string[i];
		pos += 1;
	}
	assert (pos == bytes.size());
}

std::string Helper::bytesToString(const std::vector<UC8> &bytes, int offset)
{
	int size = bytes[offset];
	offset++;
	std::string output;
	for (int i = 0; i < size; ++i)
	{
		output += bytes[offset];
		++offset;
	}
	return output;
}

void Helper::copyVectorToVector(std::vector<UC8> &copyTo, const std::vector<UC8> &copyFrom,UI32 &offsetTo)
{
	for (int i = 0; i < copyFrom.size(); ++i)
	{
		copyTo[offsetTo] = copyFrom[i];
		offsetTo++;
	}
}

void Helper::copyVectorToBackOfVector(std::vector<UC8> &copyTo, const std::vector<UC8> &copyFrom,UI32 &size)
{
	copyTo.reserve(copyTo.size() + copyFrom.size());
	for (int i = 0; i < copyFrom.size(); ++i)
	{
		copyTo.push_back(copyFrom[i]);
		++size;
	}
}

//void Helper::copyUI16ToVector(std::vector<UC8> &copyTo, const UI16 &copyFrom,UI32 &offset)
//{
//	UC8* ptr = (UC8*)&copyFrom;
//	for (int i = 0; i < sizeof(copyFrom); ++i)
//	{
//		copyTo[offset] = *ptr;
//		++ptr;
//		++offset;
//	}
//}
//
//void Helper::copyUI32ToVector(std::vector<UC8> &copyTo, const UI32 &copyFrom,UI32 &offset)
//{
//	UC8* ptr = (UC8*)&copyFrom;
//	for (int i = 0; i < sizeof(copyFrom); ++i)
//	{
//		copyTo[offset] = *ptr;
//		++ptr;
//		++offset;
//	}
//}

