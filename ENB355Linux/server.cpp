//todo #define NDEBUG
#include <assert.h>

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <fstream>

#include "Server.h"
#include "Integer.h"
#include "Single.h"
#include "String.h"
#include "Helper.h"
#include "sensor.h"
#include "motor.h"
#include "Image.h"

#include "ZephyrVisionMain.h"

#define SLEEP_TIME	200
#define PORT		3004

boost::shared_ptr<Image> Server::_theImage;

Server::Server() : terminate(false)
{
	//set up data
	setUpData();

	//generate set up packet
	generateSetUpPacket();


	//todo: start listner thread
	listener = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&Server::listnerThread, this)));

	//todo: start send thread
	sender = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&Server::sendThread, this)));

	//todo: start admin thread
	admin = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&Server::adminThread, this)));

	//todo: start update thread
	update = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&Server::updateThread, this)));
}

void Server::listnerThread()
{
	boost::asio::io_service io_service;
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), PORT);
	boost::asio::ip::tcp::acceptor acceptor(io_service, endpoint);

	while (!terminate)
	{
		std::cout << "listen\n";
		boost::asio::ip::tcp::socket *socket = new boost::asio::ip::tcp::socket(io_service);
		//try
		//{
			acceptor.accept(*socket); // 3
			boost::thread newClient(boost::bind(&Server::HandleClientComm, this, socket));
		//}
		//catch(std::exception& e) // 6
		//{
		//	std::cerr << "Exception: " << e.what() << std::endl;
		//}
			//socket->close();
		//delete socket;
	}
}

void Server::HandleClientComm(boost::asio::ip::tcp::socket *socket)
{
	std::string clientName;
	UC8 error = processnewClient(socket, clientName);

	//send login reply packet
                boost::shared_ptr<Client> loginClient;

                if (error == NoError)
                {
                    if (clientName.size() == 0)
                    {
                        clientName = socket->remote_endpoint().address().to_string();
                    }
                    //check if admin
                    if (ADMIN_GCS_NAME == clientName)
                    {
                        error = AdminLogin;
                        if (adminClient != NULL && adminClient->getSocket()->is_open())
                        {
                            adminClient->getSocket()->close();
                        }
                        loginClient = boost::shared_ptr<Client>(new Client(socket, true, clientName, socket->remote_endpoint().address().to_string(), commandLog.badCommand()));
                        adminClient = loginClient;
                        commandLog.add(GCS, std::string("Active GCS Sent Login Request from: ") + loginClient->getIP());
                    }
                    else
                    {
                        loginClient = boost::shared_ptr<Client>(new Client(socket, false, clientName, socket->remote_endpoint().address().to_string(), commandLog.badCommand()));
                    }
                }


                std::vector<UC8> reply;
				reply.resize(1 + 4 + 1 + 1);
                UI32 offset = 0;
				reply[offset] = ReplyLoginRequest;
				++offset;
				Helper::copyXToVector<UI32>(reply, reply.size(), offset);
                //BitConverter.GetBytes(reply.Length).CopyTo(reply, 1);
                reply[offset] = error;
				++offset;
                reply[offset] = KEEP_ALIVE_TIME_IN_SECONDS;

				boost::asio::write(*socket, boost::asio::buffer(reply));
                //clientStream.Write(reply, 0, reply.Length);

                //send set up and add to list
                if (error == NoError || error == AdminLogin)
                {
                    boost::asio::write(*socket, boost::asio::buffer(_setUpPacket));

                    clients.push_back(loginClient);
                    std::cout << "Client Connected: " << clientName << "\nClient IP: " << loginClient->getIP() << std::endl;
                    if (loginClient->getIsActive())
                    {
                        commandLog.add(Blimp, "Accepted login Active Login Request from: " + loginClient->getIsActive());
                    }
                }
                else
                {
                    loginClient->getSocket()->close();
                }

	//todo Check for errors
}

UC8 Server::processnewClient(boost::asio::ip::tcp::socket *socket, std::string &ClientName)
{
	UC8 error = (UC8)NoError;
    ClientName = "";
    try
    {
        //get login packet
        std::vector<UC8> buffer;
		buffer.resize(5);
		//todo check for errors
        boost::asio::read(*socket, boost::asio::buffer(buffer));
		UI32 offset = 0;
        UC8 packetHeader = buffer[offset];
		++offset;
        if (packetHeader != (UC8)LoginRequest)
        {
            return error = UnknownTypeOfPacket;
        }

        UI32 size = Helper::getXfromVector<UI32>(buffer, offset);
        if (size < 6)
        {
            return error = DataPacketSizeMismatch;
        }
		buffer.clear();
		buffer.resize(size - 5);
        boost::asio::read(*socket, boost::asio::buffer(buffer));
        int pos = 0;
		offset = 0;
        ClientName = Helper::bytesToString(buffer, offset);

        if (buffer.size() != ClientName.size()+1)
        {
            return error = (UC8)DataPacketSizeMismatch;
        }
    }
    catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
        return error = (UC8)DataPacketSizeMismatch;
    }

	return error;
}

std::vector<UC8> Server::generateCommandsPacket(std::list<std::vector<UC8> >::iterator &lastCommand)
{
    std::vector<UC8> commandLogBody = commandLog.toBytes(lastCommand);

    if (commandLogBody.size() == 0)
    {
        return commandLogBody;
    }
    UI32 packetSize = (UI32)commandLogBody.size() + 5;
    std::vector<UC8> packet;
    packet.resize(packetSize);
    UI32 pos = 0;
    packet[pos] = CommandsPacket;
    pos +=1;
    Helper::copyXToVector<UI32>(packet, packetSize, pos);

    Helper::copyVectorToVector(packet,commandLogBody,pos);
    return packet;
}

void Server::sendThread()
{
	std::list<std::vector<UC8> >::iterator lastCommand = commandLog.badCommand();
	std::vector<UC8> commandsPacket;
	while (!terminate)
	{
		//todo everything
		//todo command stuff
		if (clients.size() > 0)
        {
            //generate packet
            generateDataPacket();
            std::list<std::vector<UC8> >::iterator previousCommand = lastCommand;
            commandsPacket.resize(0);


            //send packet
            //NetworkStream clientStream;
			std::list<boost::shared_ptr<Client> >::iterator clientNode = clients.begin();
            while (clientNode != clients.end())
            {
				bool increment = true;
                if ((**clientNode).getSocket()->is_open())
                {
                    try
                    {
                        boost::asio::write(*(**clientNode).getSocket(), boost::asio::buffer(_dataPacket));

						if ((**clientNode).getLastCommand() == previousCommand)
                        {
                            if (commandsPacket.size() == 0)
                            {
                                //only generate if needed and only needs to be generated once
                                commandsPacket = generateCommandsPacket(lastCommand);
                            }
                            if (commandsPacket.size() > 0)
                            {
                                boost::asio::write(*(**clientNode).getSocket(), boost::asio::buffer(commandsPacket));
                            }
                            (**clientNode).setLastCommand(lastCommand);
                        }
                        else
                        {
                            std::list<std::vector<UC8> >::iterator tempClientLastCommand = (**clientNode).getLastCommand();
                            std::vector<UC8> IndividualCommandsPacket = generateCommandsPacket(tempClientLastCommand);
                            (**clientNode).setLastCommand(tempClientLastCommand);
                            if (IndividualCommandsPacket.size() > 0)
                            {
                                boost::asio::write(*(**clientNode).getSocket(), boost::asio::buffer(IndividualCommandsPacket));
                            }
                        }

                    }
                    catch (std::exception& e)
					{
						std::cerr << "error with client" << e.what() << std::endl;
                        std::cerr << "Error with: " << (**clientNode).getName() << std::endl;
                        std::cerr << "Disconnected: " << (**clientNode).getName() << std::endl;
                        if ((**clientNode).getIsActive())
						{
							commandLog.add(Blimp, "Active GCS Disconnected: " + (**clientNode).getIP());
						}
                        (**clientNode).getSocket()->close();
						clientNode = clients.erase(clientNode);//todo check this fixes delete inside of the loop
						increment = false;
                    }
                }
                else
                {
                    std::cout << "Disconnected: " << (**clientNode).getName() << std::endl;
                    if ((**clientNode).getIsActive())
                    {
                        commandLog.add(Blimp, "Active GCS Disconnected: " + (**clientNode).getIP());
                    }
                    (**clientNode).getSocket()->close();
					clientNode = clients.erase(clientNode);//todo check this fixes delete inside of the loop
					increment = false;
                }
				if (increment) {
					clientNode++;
				}
            }


        }
		boost::this_thread::sleep(boost::posix_time::milliseconds(SLEEP_TIME));
	}
}

void Server::adminThread()
{
	while (!terminate)
	{
		//todo
		//std::cout << "admin\n";
		if (adminClient.get() != NULL && adminClient->getSocket()->is_open())
		{
		try
		{
			//get packet
			std::vector<UC8> buffer;
			buffer.resize(5);
			//todo check for errors
			boost::asio::read(*(adminClient->getSocket()), boost::asio::buffer(buffer));
			UI32 offset = 0;
			UC8 packetHeader = buffer[offset];
			++offset;
			UI32 size = Helper::getXfromVector<UI32>(buffer, offset);
			buffer.resize(size - 5);
			boost::asio::read(*(adminClient->getSocket()), boost::asio::buffer(buffer));
			int pos = 0;
			offset = 0;
			if (packetHeader == (UC8)Control)
			{
				//todo error checking
				float leftMotor, rightMotor, verticalMotor;

				leftMotor = Helper::getXfromVector<float>(buffer, offset);
				rightMotor = Helper::getXfromVector<float>(buffer, offset);
				verticalMotor = Helper::getXfromVector<float>(buffer, offset);

				Motor::updateLeftMotor(leftMotor);
				Motor::updateRightMotor(rightMotor);
				Motor::updateVericalMotor(verticalMotor);
				//std::cout << "L: " << leftMotor << ", R: " << rightMotor << ", V: " << verticalMotor << std::endl;
			}
			else if (packetHeader == (UC8)DeliverTo)
			{
				std::cout << "Delivering To: " << (int)buffer[0] << std::endl;
				if (buffer[0] >= 0 && buffer[0] <= 2)
				{
					survivorToDeliver(buffer[0]);
					commandLog.add(GCS, "Deliver to survivor");
					commandLog.add(Blimp, "Commadn Accepted");
				}
				else
				{
					Motor::deliverPayload();
commandLog.add(GCS, "Deliver Payload Now");
commandLog.add(Blimp, "Commadn accepted");
				}
			}
			else
			{
				std::cout << "packet header:" << packetHeader << std::endl;
			}
		}
		catch (std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
		}
		else
		{
			boost::this_thread::sleep(boost::posix_time::milliseconds(SLEEP_TIME));
		}
	}
}

void Server::stop()
{
	terminate = true;
	listener->join();
	sender->join();
	admin->join();
}



void Server::updateThread()
{
    while (!terminate)
    {
        //todo

        (dynamic_cast<Single*>(&*_data[1]))->update(Sensors::front()+FRONT_OFFSET);
        (dynamic_cast<Single*>(&*_data[2]))->update(Sensors::right()+SIDE_OFFSET);
        (dynamic_cast<Single*>(&*_data[3]))->update(Sensors::left()+SIDE_OFFSET);
        (dynamic_cast<Single*>(&*_data[4]))->update(Sensors::top()+BOTTOM_OFFSET);

	bool *survivorDetected;

	survivorInfo(&survivorDetected);

        boost::this_thread::sleep(boost::posix_time::milliseconds(SLEEP_TIME));

	for (int i = 0; i < NUM_SURVIVORS; ++i)
	{
		if (survivorDetected[i])
		{
			(dynamic_cast<String*>(&*_data[5 + i]))->update("Yes");
		}
	}
    }

}

void Server::setUpData()
{
	_data.reserve(3);
	_data.push_back(ObjectPtr(new String("System Status")));
	_data.push_back(ObjectPtr(new Single("Front (cm)", 40+FRONT_OFFSET, 140+FRONT_OFFSET)));
	_data.push_back(ObjectPtr(new Single("Right (cm)", 40+SIDE_OFFSET, 140+SIDE_OFFSET)));
	_data.push_back(ObjectPtr(new Single("Left (cm)", 40+SIDE_OFFSET, 140+SIDE_OFFSET)));
	_data.push_back(ObjectPtr(new Single("Bottom (cm)", 40+BOTTOM_OFFSET, 140+BOTTOM_OFFSET)));
	_data.push_back(ObjectPtr(new String("Yellow Survivor Detected")));
	_data.push_back(ObjectPtr(new String("Green Survivor Detected")));
	_data.push_back(ObjectPtr(new String("Pink Survivor Detected")));
	_data.push_back(ObjectPtr(new Image("Blimp Image")));

	(dynamic_cast<String*>(&*_data[0]))->update("name");
	(dynamic_cast<Single*>(&*_data[1]))->update(0.0f);
	(dynamic_cast<Single*>(&*_data[2]))->update(0.0f);
	(dynamic_cast<Single*>(&*_data[3]))->update(0.0f);
	(dynamic_cast<Single*>(&*_data[4]))->update(0.0f);

	for (int i = 0; i < NUM_SURVIVORS; ++i)
	{
		(dynamic_cast<String*>(&*_data[5 + i]))->update("No");
	}

	(dynamic_cast<Image*>(&*_data[5 + NUM_SURVIVORS]))->update("compiling.jpg");
	_theImage =  boost::dynamic_pointer_cast<Image>(_data[5 + NUM_SURVIVORS]);
	commandLog.add(Blimp, "Server Started");
}

void Server::generateSetUpPacket()
{
	assert(_data.size() <= UC8_MAX);
	UI32 packetSize = 5 + 1;
	packetSize+=_data.size();
	for (int i = 0; i < _data.size(); ++i)
	{
		packetSize += (*_data[i]).getMaxBytes().size();
		packetSize += (*_data[i]).getMinBytes().size();
		packetSize += (*_data[i]).getNameBytes().size();
	}
	_setUpPacket.resize(packetSize);
	UI32 pos = 0;
	_setUpPacket[pos] = SensorDataStructure;
	pos+=1;

	//add size to packet
	Helper::copyXToVector<UI32>(_setUpPacket, packetSize, pos);

	//add number of data bits to packet
	_setUpPacket[pos] = (UC8)_data.size();
	pos++;

	for (int i = 0; i < _data.size(); ++i)
	{
		_setUpPacket[pos] = (*_data[i]).getDataType();
		pos++;
	}
	//get limits set up
	for (int i = 0; i < _data.size(); ++i)
	{
		Helper::copyVectorToVector(_setUpPacket,(*_data[i]).getMinBytes(),pos);
		Helper::copyVectorToVector(_setUpPacket,(*_data[i]).getMaxBytes(),pos);
	}
	//get names copyed across
	for (int i = 0; i < _data.size(); ++i)
	{
		Helper::copyVectorToVector(_setUpPacket,(*_data[i]).getNameBytes(), pos);
	}
	assert(pos == packetSize);
}

void Server::generateDataPacket()
{
	_dataPacket.resize(5);
	UI32 packetSize = 5;
	UI32 pos = 0;
	_dataPacket[pos] = SensorData;
	pos++;
	for (int i = 0; i < _data.size(); ++i)
	{
	    if ((*_data[i]).getDataType() == STRING_DATA_TYPE)
	    {
	        std::vector<UC8> bytes = (*_data[i]).getBytes();
	        UI32 size = bytes.size();
	        _dataPacket.resize(_dataPacket.size() + UI32_BYTES);
	        Helper::copyXToVector<UI32>(_dataPacket, size, packetSize);
	        Helper::copyVectorToBackOfVector(_dataPacket, bytes, packetSize);
	    }
		else if ((*_data[i]).getDataType() == IMAGE_DATA_TYPE)
		{
			(*_data[i]).Lock();
			Helper::copyVectorToBackOfVector(_dataPacket,(*_data[i]).getBytes(), packetSize);
			(*_data[i]).Unlock();
		}
	    else
	    {
	        Helper::copyVectorToBackOfVector(_dataPacket,(*_data[i]).getBytes(), packetSize);
	    }
	}
	//add size to packet
	Helper::copyXToVector<UI32>(_dataPacket, packetSize, pos);
}

Image* Server::getTheImage()
{
	return &*_theImage;
}
