#ifndef _STRING_H_
#define _STRING_H_

#include <string>
#include <vector>
#include "Object.h"
#include "DataTypes.h"

class String : public Object
{
protected:
	std::string _string;
public:
	String(std::string name);
	std::vector<UC8> &getBytes();
	void update(std::string string);
	void operator=(std::string string);
};

#endif